bootstrapSlides();

function bootstrapSlides() {
  var containersList = document.getElementsByClassName("slideshow-container");

  for (
    var containerIndex = 0;
    containerIndex < containersList.length;
    containerIndex++
  ) {
    let slideContainer = containersList[containerIndex];

    let slides = slideContainer.getElementsByClassName("slide");
    let active = slideContainer.getElementsByClassName("active")[0];

    for (slideIndex = 0; slideIndex < slides.length; slideIndex++) {
      let slide = slides[slideIndex];
      slide.style.display = "none";
    }

    slides[0].style.display = "block";

    active.innerHTML = 0;
  }
}

function moveSlide(e) {
  let action;

  if (e.classList.contains("prev")) {
    action = "prev";
  } else {
    action = "next";
  }

  let slideContainer = e.parentElement;

  let slides = slideContainer.getElementsByClassName("slide");
  let active = slideContainer.getElementsByClassName("active")[0];
  let activeNumber = parseInt(active.innerHTML);

  for (slideIndex = 0; slideIndex < slides.length; slideIndex++) {
    let slide = slides[slideIndex];
    slide.style.display = "none";
  }

  if (action === "prev") {
    if (activeNumber <= 0) {
      activeNumber = slides.length - 1;
    } else {
      activeNumber = activeNumber - 1;
    }
  } else {
    if (activeNumber < slides.length - 1) {
      activeNumber = activeNumber + 1;
    } else {
      activeNumber = 0;
    }
  }

  slides[activeNumber].style.display = "block";

  active.innerHTML = activeNumber;
}
